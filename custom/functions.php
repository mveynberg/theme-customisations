<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */


add_filter('woocommerce_product_query_meta_query', 'custom_shop_only_priced_products', 10, 2);
function custom_shop_only_priced_products($meta_query, $query) {
    if( is_admin() ) return $meta_query;

    $meta_query[] = array(
        'key'     => '_price',
        'value'   => 0,
        'type'    => 'numeric',
        'compare' => '>'
    );
    return $meta_query;
}

add_action('woocommerce_add_to_cart', 'custom_add_free_product', 99, 2);
function custom_add_free_product($cart_item_key, $product_id) {
    global $woocommerce;

    $products_sum = 0;
    $added_already = false;

    foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
        if($values['data']->get_id() == 54) {
            $added_already = $cart_item_key;
        }
        $products_sum += $values['data']->price*$values['quantity'];
    }

    if ($products_sum >= 1000 && !$added_already && $product_id != 54) {
        $woocommerce->cart->add_to_cart(54);
    }
}

add_action('woocommerce_cart_item_removed', 'custom_remove_free_product', 99, 1);
function custom_remove_free_product($cart_item_key) {
    global $woocommerce;

    $products_sum = 0;
    $added_already = false;

    foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
        if($values['data']->get_id() == 54) {
            $added_already = $cart_item_key;
        }
        $products_sum += $values['data']->price*$values['quantity'];
    }

    if ($products_sum < 1000 && $added_already) {
        $woocommerce->cart->remove_cart_item($added_already);
    }
}
